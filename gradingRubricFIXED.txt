Corey Salzer and James Fitzhugh
Creative Project Grading Rubric
4/22/15


MEAN Stack Implementation (40 points)
  -MEAN Stack Installation and Setup (27 points)
    - A node server is running (5 points)
    - Express and Mongoose Installed (5 points)
    - MongoDB is running (7 points)
    - MEAN Stack configured (8 points)
  -MongoDB (13 points)
    - Database is set up (5 points)
    - Collections are added (2 points)
    - Admin user is set (3 point)
    - Mongo Express installed and running (5 points)
  
Site Management (45 points)
 - Users are able to browse through workouts fom the database (10 points)
 - Users can login in as an existing user (6 points)
 - Users can register as a new user (6 points)
 - Users can choose which workouts they have completed (13 points)
 - Users can see the progress of other users (10 points)


Best Practices (5 Points)
- Code is well formatted and easy to read, with proper commenting (2 points)
- Code passes HTML validation (3 points)
Usability (5 Points):
- Sit is easy and intuitive (4 points)
- Site is visually appealing (1 point)
Creative Portion (5 points)
