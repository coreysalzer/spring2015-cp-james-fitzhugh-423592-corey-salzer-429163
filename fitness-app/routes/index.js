var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Workout = mongoose.model('Workout');
var User = mongoose.model('User');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/workouts', function(req, res, next) {
  Workout.find(function(err, workouts){
    if(err){ return next(err); }
    res.json(workouts);
  });
});

router.post('/workouts', function(req, res, next) {
  var workout = new Workout(req.body);

  workout.save(function(err, workout){
    if(err){ return next(err); }

    res.json(workout);
  });
});

router.get('/usernames', function(req, res, next) {
     User.find(function(err, users){
       if(err){ return next(err); }
       res.json(users);
     });
});

router.post('/login', function(req, res, next) {
  var user = new User(req.body);

  user.save(function(err, user){
    if(err){ return next(err); }

    res.json(user);
  });
});

router.post('/addWorkout', function(req, res, next) {
  var tempWorkout = new Workout(req.body.workout);
  var tempUser = new User(req.body.user);
  console.log(tempUser.completedWorkouts);
  //tempUser.completedWorkouts.push(tempWorkout._id);
  //var workouts = tempUser.completedWorkouts;
  //console.log(workouts);
  User.update({_id: tempUser._id}, {$set: {completedWorkouts: tempUser.completedWorkouts}}, function(err, workout) {
  	if(err){return next(err);}
	res.json(workout);
  });
});
module.exports = router;
 
