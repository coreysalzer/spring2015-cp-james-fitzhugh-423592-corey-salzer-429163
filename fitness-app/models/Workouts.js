var mongoose = require('mongoose');

var WorkoutSchema = new mongoose.Schema({
  name: String,
  description: String,
  video: String,
});

mongoose.model('Workout', WorkoutSchema);
