'use strict';

/* Controllers */

var fitnessControllers = angular.module('fitnessControllers', []);

fitnessControllers.controller('WorkoutListCtrl', ['$scope','Workout', 'workouts', 'users', 'login', 'addWorkout',
  function($scope, Workout, workouts, users, login, addWorkout) {
    $scope.loggedIn = false;
    $scope.workouts = workouts.workouts;
    $scope.users = users.users;
    $scope.username = "";
    $scope.logIn = function(){
      if($scope.loggedIn || $scope.usernameInput == "") {
         return;
      }
      for(var i = 0; i< $scope.users.length; i++) {
            if($scope.users[i].name == $scope.usernameInput) {
               $scope.loggedIn = true;
	       $scope.username = $scope.usernameInput;
	       $scope.usernameInput = "";
	       return;
            }
        }
      $scope.username = $scope.usernameInput;
      $scope.usernameInput = "";
      login.addUser({
          name: $scope.username,
          completedWorkouts: []
      });
     $scope.users.push({
          name: $scope.username,
          completedWorkouts: []
     }); 
     $scope.loggedIn = true;
    }
    $scope.checkIfCompleted = function(workoutId) {
    	var user = null;
	for(var i = 0; i < $scope.users.length; i++){
	    if($scope.users[i].name == $scope.username) {
               user = $scope.users[i];
            }
	}
	if(user == null) {
		return false;
	}
	for(var i = 0; i < user.completedWorkouts.length; i++){
	   if(user.completedWorkouts[i] == workoutId) {
	   	return true;
	   }
	}
	return false;
    }
    $scope.addWorkout = function(workoutId){
	var workout = null;
    	for(var i = 0; i< $scope.workouts.length; i++) {
            if($scope.workouts[i]._id == workoutId) {
	       workout = $scope.workouts[i];
            }
        }
	var user = null;
	var index = -1;
        for(var i = 0; i< $scope.users.length; i++) {
            if($scope.users[i].name == $scope.username) {
               user = $scope.users[i];
		index = i;
            }
        }
	if(user == null) {
	    return;
	}
	 for(var i = 0; i< user.completedWorkouts.length; i++) {
            if(user.completedWorkouts[i] == workoutId) {
               return;
            }
        }
        addWorkout.addWorkout(workout, user);
	if(index!=-1){
	    if($scope.users[index].completedWorkouts.length == 0){
	       $scope.users[index].completedWorkouts = [workout._id];
  	    }
	    else{
	       $scope.users[index].completedWorkouts.push(workout._id);
	    }
	}
    }
    $scope.computePercentage = function(username){
    	var user = null;
        for(var i = 0; i< $scope.users.length; i++) {
            if($scope.users[i].name == username) {
               user = $scope.users[i];
            }
        }
	if(user != null) {
	    return parseFloat((user.completedWorkouts.length))/workouts.workouts.length*100;
	}
    }
  }]);

