'use strict';

/* Services */

var fitnessServices = angular.module('fitnessServices', ['ngResource']);

fitnessServices.factory('Workout', ['$resource',
  function($resource){
    return $resource('phones/:phoneId.json', {}, {
      query: {method:'GET', params:{phoneId:'phones'}, isArray:true}
    });
  }]);

fitnessServices.factory('workouts', ['$http', function($http){
   var workouts = {
      workouts: []
   };
   workouts.getWorkouts = function() {
        return $http.get('/workouts').success(function(data){
            angular.copy(data, workouts.workouts);
        });
    };
   return workouts;
}])

fitnessServices.factory('users', ['$http', function($http){
   var users = {
      users: []
   };
   users.getUsers = function() {
        return $http.get('/usernames').success(function(data){
            angular.copy(data, users.users);
        });
    };
   return users;
}])

fitnessServices.factory('login', ['$http', function($http){
   var login = {};
   login.addUser = function(user){
      return $http.post('/login', user).success(function(data) {
        //login.push(data);
      });
   };
   return login;
}])

fitnessServices.factory('addWorkout', ['$http', function($http){
   var workout = {};
   workout.addWorkout = function(workout, user){
      return $http.post('/addWorkout', {workout, user}).success(function(data) {
    
      });
    };
    return workout;
}])
