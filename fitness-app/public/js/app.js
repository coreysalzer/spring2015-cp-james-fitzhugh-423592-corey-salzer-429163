'use strict';

/* App Module */

var fitnessApp = angular.module('fitnessApp', [
  'ngRoute',
  'fitnessAnimations',

  'fitnessControllers',
  'fitnessFilters',
  'fitnessServices'
]);


fitnessApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/workouts', {
        templateUrl: 'partials/workout-list.html',
        controller: 'WorkoutListCtrl',
        resolve: {
           workoutList: function(workouts){
	      return workouts.getWorkouts();
	   },
	   userList: function(users){
	      return users.getUsers();
	   }
        }
      }).
      otherwise({
        redirectTo: '/workouts'
      });
  }]);
